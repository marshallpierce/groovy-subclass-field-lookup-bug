package org.mpierce.groovy

class Foo {

  final String fooField = "foo string"

  boolean methodWithAnonSubclass() {
    new Bar() {
      @Override
      void overrideMe() {
        assert fooField != null
      }
    }.overrideMe()
    return true
  }
}
