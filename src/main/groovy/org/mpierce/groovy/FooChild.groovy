package org.mpierce.groovy

class FooChild extends Foo {

  boolean childMethodWithAnonSubclass() {
    new Bar() {
      @Override
      void overrideMe() {
        assert fooField != null
      }
    }.overrideMe()
    return true
  }
}
