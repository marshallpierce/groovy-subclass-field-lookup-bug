package org.mpierce.groovy

import org.junit.Test

class FieldResolutionTest {

  @Test
  public void testWorksWhenCallingParentMethodOnChild() {
    assert new FooChild().methodWithAnonSubclass()
  }

  @Test
  public void testWorksWhenCallingChildMethodOnChild() {
    assert new FooChild().childMethodWithAnonSubclass()
  }

  @Test
  public void testWorksWhenCallingParentMethodOnParentClass() {
    assert new Foo().methodWithAnonSubclass()
  }
}
